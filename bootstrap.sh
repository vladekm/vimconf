#!/bin/bash
set -x
cd ~/
ln -sn ~/dotfiles/vimconf/vim/ ~/.vim
ln -s ~/dotfiles/vimconf/vimrc ~/.vimrc
cd ~/dotfiles/vimconf
git submodule init && git submodule update
